LCFG Ubuntu Packagelists
========================
This repository holds all the inputs to generate the LCFG Ubuntu package lists. Input yaml files are in the `focal` folder. The minimal package lists only get the `focal/geos_options_min.yaml` and all `focal/geos_01_*.yaml` files. The general package list is constructed from the `focal/geos_options.yaml` and both `focal/geos_01_*.yaml` and `focal/geos_02_*.yaml` files. 

Run `make` to regenerate the `geos_ubu2004_general.pkgs` and `geos_ubu2004_minimal.pkgs`. These files should get copied into the `geos_packagelists` repository. The Makefile assumes that a Ubuntu chroot exists in `/scratch/local/soy/` and that the core LCFG repository is checked out in the parent directory.

Packages are separated into thematic groups spread over various input yaml files.
