focal = geos_ubu2004_minimal.pkgs geos_ubu2004_general.pkgs 

SOY = lcfg-soy -p geos -r deb
FOCALSOY = $(SOY) -c /scratch/local/soy/focal/ ../core/packages/lcfg/lcfg_ubu2004_options.pkgs

all:	$(focal)

%.pkgs:	%.pkgs.in
	sed 's|/all|/noarch|' $< > $@

geos_ubu2004_minimal.pkgs.in:	focal/geos_options_min.yaml focal/geos_01_*.yaml
	$(FOCALSOY)  $^ -o $@

geos_ubu2004_general.pkgs.in:	focal/geos_options.yaml focal/geos_0?_*.yaml
	$(FOCALSOY)  ../core/packages/lcfg/lcfg_ubu2004_desktop.pkgs ../core/soy/lcfg/lcfg_base.yaml $^ -o $@
